// ==UserScript==
// @name         Corriere della Sera - unblock AdBlock and avoid modals
// @namespace    http://tampermonkey.net/
// @version      0.4
// @description  Removes AdBlock blocks and automaticcaly hides blocking modal dialogs
// @author       mous16
// @match        https://*.corriere.it/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=corriere.it
// @license      ICS
// @grant        none
// ==/UserScript==

(function userScript () {
    const removeClass = function (target, className) {
        const doRemoveClass = function (node) {
            if (!node.classList || !node.classList.contains(className)) {
                return false;
            }

            node.classList.remove(className);
            return true;
        };
        new MutationObserver((mutationList) => {
            for (const mutation of mutationList) {
                if (mutation.type !== "attributes") {
                    continue;
                }

                if (mutation.attributeName !== "class") {
                    continue;
                }

                doRemoveClass(mutation.target);
            }
        }).observe(
            target,
            {"attributes": true}
        );

        doRemoveClass(target);
    };

    const removeChildrenWithClass = function (target, className) {
        const doRemoveNode = function (node) {
            if (!node.classList ||
                        !node.classList.contains(className) ||
                        !node.parentElement) {
                return false;
            }

            node.parentElement.removeChild(node);
            return true;
        };

        const doRemoveNodeRecursive = function (node) {
            if (doRemoveNode(node)) {
                return true;
            }

            const queryRes = document.evaluate(
                `.//*[contains(@class, "${className}")]`,
                node,
                null,
                XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE,
                null
            );
            let removed = false;
            for (let idx = 0; idx < queryRes.snapshotLength; idx++) {
                const queryNode = queryRes.snapshotItem(idx);
                if (!queryNode.parentElement) {
                    continue;
                }
                queryNode.parentElement.removeChild(queryNode);
                removed = true;
            }
            return removed;
        };


        new MutationObserver((mutationList) => {
            for (const mutation of mutationList) {
                switch (mutation.type) {
                case "attributes":
                    if (mutation.attributeName !== "class") {
                        break;
                    }

                    doRemoveNode(mutation.target);
                    break;
                case "childList":
                    for (const addedNode of mutation.addedNodes) {
                        doRemoveNodeRecursive(addedNode);
                    }
                    break;

                default: break;
                }
            }
        }).observe(
            target,
            {
                "attributeFilter": ["class"],
                "childList": true,
                "subtree": true
            }
        );

        doRemoveNodeRecursive(target);
    };

    removeClass(
        document.documentElement,
        "has--adblock"
    );
    removeChildrenWithClass(
        document.body,
        "bck-adblock"
    );

    removeClass(
        document.body,
        "tp-modal-open"
    );
    removeChildrenWithClass(
        document.body,
        "tp-modal"
    );
    removeChildrenWithClass(
        document.body,
        "tp-backdrop"
    );
}());
