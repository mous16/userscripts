// ==UserScript==
// @name         Skillacloud - easier learning
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Allow platform usage with less limitations
// @author       mous16
// @match        https://www.skillacloud.com/pluginfile.php/*/mod_scorm/content/*/index.html
// @icon         https://www.skillacloud.com/pluginfile.php/1/theme_skilla/favicon/1702489598/favicon.ico
// @license      ICS
// @grant        none
// ==/UserScript==

(function userScript () {
    window.setInterval(
        () => {
            /* global app */

            if (!app || !app.settings) {
                return false;
            }

            if (app.settings.stopVideoOnFocuseLeave === "false") {
                return true;
            }

            app.settings.stopVideoOnFocuseLeave = "false";
            return true;
        },
        5000
    );
}());
